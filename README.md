# arch-base
![Docker Pulls](https://img.shields.io/docker/pulls/forumi0721/arch-base)
![Docker Stars](https://img.shields.io/docker/stars/forumi0721/arch-base)

### x64
![Docker Image Size](https://img.shields.io/docker/image-size/forumi0721/arch-base/x64)
### aarch64
![Docker Image Size](https://img.shields.io/docker/image-size/forumi0721/arch-base/aarch64)
### armv7
![Docker Image Size](https://img.shields.io/docker/image-size/forumi0721/arch-base/armv7)



----------------------------------------
#### Description

* Distribution : [Arch Linux](https://www.archlinux.org/)
* Architecture : x64,aarch64,armv7
* Appplication : -



----------------------------------------
#### Run

```sh
docker run -i -t --rm \
           forumi0721/arch-base:[ARCH_TAG]
```



----------------------------------------
#### Usage

```dockerfile
FROM forumi0721/arch-base:[ARCH_TAG]

RUN 'build-code'
```



----------------------------------------
#### Docker Options

| Option             | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Ports

| Port               | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Volumes

| Volume             | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Environment Variables

| ENV                | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |

